#!/bin/bash 
. $(dirname "${BASH_SOURCE[0]}")/../openxal-environment.sh
java -cp "$OPENXAL_HOME/lib/openxal/openxal.library-$OPENXAL_VERSION.jar" org.openepics.discs.exporters.TraceWinExporter "$@"

