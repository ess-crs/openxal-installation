#!/bin/bash
#!-*- sh -*-
#
# Abs: meme-rdb-client.sh executes a client side program of the MEME rdb service. 
#
# Rem: rdb is a simple EPICS v4 RPC service client for gettig 
#      data out of a relational database like Oracle.
#
# Args: $1 the name of a query (a SQL SELECT statement) to execute in
#          the db. This must be a name in the RdbService's list of names of
#          queries.
#       $2 whether you want column or row oriented output (column
#          is default) 
#       $3 whether you want headings (the names of the db
#          columns) in your printout
#
# Usage: 
#
#        Execute meme-rdb-client.sh anytime while meme-rdb-server*.sh is running. It
#        takes one non-optional argument, being the name of a query that itself
#        is understood by your rdbServer to identify a SQL select statement. 
# 
#         > ./meme-rdb-client.sh name-of-query-understood-by-your-server
#
#         Examples: 
#         bash-3.2$ ./meme-rdb-client.sh ESS/elementInfo.byS
#                   ./meme-rdb-client.sh sls:allmagnetdata col nolabels 
#                   
# 
# Ref: <TODO>
# ----------------------------------------------------------------------------
# Auth: 14-Sep-2011, Greg White (greg@slac.stanford.edu)
# Mod:  12-Mar-2013, Greg White (greg@slac.stanford.edu, gregory.white@psi.ch)
#       Changed way of finding a test/development version of rdb_setup.bash
#       so as to use PATH 
#       07-Feb-2012, Greg White (greg@slac.stanford.edu, gregory.white@psi.ch)
#       bugfix: replaced -server with -client java command line opt. 
# ============================================================================

. $(dirname "${BASH_SOURCE[0]}")/../openxal-environment.sh

# Which rdbServer is now determined by envs_*.bash files
# 
# If the client is being executed in "DEV" mode, then use the 
# development server and specific port number.
#
#if [ x"$MEME_MODE" = x"DEV" ]; then 
#    export MEME_RDB_SERVER_BROADCAST_PORT=47239
#    export EPICS_PVA_ADDR_LIST=lcls-dev1.slac.stanford.edu:${MEME_RDB_SERVER_BROADCAST_PORT}
#fi

# Execute the client of the service, passing arguments on.
#
java -client \
   -cp "$OPENXAL_HOME/lib/openxal/openxal.apps.meme.rdb-$OPENXAL_VERSION.jar" \
   edu.stanford.slac.meme.service.rdb.RdbClient $1 ${2:-col} ${3:-labels}

exit $?

