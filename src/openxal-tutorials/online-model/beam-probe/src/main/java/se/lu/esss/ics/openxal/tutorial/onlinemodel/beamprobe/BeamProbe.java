package se.lu.esss.ics.openxal.tutorial.onlinemodel.beamprobe;

import xal.model.alg.EnvelopeTracker;
import xal.model.probe.EnvelopeProbe;
import xal.sim.scenario.ProbeFactory;
import xal.smf.Accelerator;
import xal.smf.AcceleratorSeq;
import xal.smf.data.XMLDataManager;

public class BeamProbe {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String sequenceName = "MEBT";
		
		Accelerator accelerator = XMLDataManager.acceleratorWithPath("../config/main.xal");
		AcceleratorSeq sequence = accelerator.getSequence(sequenceName);
		
		EnvelopeTracker tracker = new EnvelopeTracker();
		EnvelopeProbe probe = ProbeFactory.getEnvelopeProbe(sequence, tracker);

		probe.setBeamCurrent(0.);
		probe.setKineticEnergy(885.e6);
		
		double bunchFrequency = probe.getBunchFrequency();
		System.out.println("Bunch Frequency:" + bunchFrequency + "\n");
	}

}
