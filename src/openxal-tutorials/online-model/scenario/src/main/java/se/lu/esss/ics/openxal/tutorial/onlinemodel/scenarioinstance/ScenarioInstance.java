package se.lu.esss.ics.openxal.tutorial.onlinemodel.scenarioinstance;

import xal.model.ModelException;
import xal.model.alg.EnvelopeTracker;
import xal.model.probe.EnvelopeProbe;
import xal.sim.scenario.ProbeFactory;
import xal.smf.Accelerator;
import xal.smf.AcceleratorSeq;
import xal.smf.data.XMLDataManager;
import xal.sim.scenario.Scenario;

public class ScenarioInstance {

	/**
	 * @param args
	 * @throws ModelException 
	 */
	public static void main(String[] args) throws ModelException {
		// Load the SMF 
		Accelerator accelerator = XMLDataManager.acceleratorWithPath("../config/main.xal");
		// Define the sequence one is interesting in, in this example MEBT sequence from the SNS accelerator
		String sequenceName = "MEBT";
		// Select only the sequence one is interesting in, from all the components in the accelerator
		AcceleratorSeq sequence = accelerator.getSequence(sequenceName);	
		
		// Instantiation of the scenario object for a given sequence
		Scenario model = Scenario.newScenarioFor(sequence);

		// Setting up a valid algorithm dor the model and loading a sequence
		EnvelopeTracker tracker = new EnvelopeTracker();
		EnvelopeProbe probe = ProbeFactory.getEnvelopeProbe(sequence, tracker);
		model.setProbe(probe);
		model.setSynchronizationMode(Scenario.SYNC_MODE_DESIGN);
		
		// Setting the start and end point of the propagation of the model through the sequence
		// Define start and end point of the simulation
		String start = "MEBT_Mag:QH01";
		String end = "MEBT_Mag:QH05";
		model.setStartNode(start);
		model.setStopNode(end);
	}

}
