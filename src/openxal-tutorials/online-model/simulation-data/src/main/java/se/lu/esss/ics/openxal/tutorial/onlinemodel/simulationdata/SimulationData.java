package se.lu.esss.ics.openxal.tutorial.onlinemodel.simulationdata;

import java.util.Iterator;

import xal.model.ModelException;
import xal.model.alg.EnvelopeTracker;
import xal.model.probe.EnvelopeProbe;
import xal.model.probe.traj.ProbeState;
import xal.model.probe.traj.Trajectory;
import xal.sim.scenario.ProbeFactory;
import xal.sim.scenario.Scenario;
import xal.smf.Accelerator;
import xal.smf.AcceleratorSeq;
import xal.smf.data.XMLDataManager;

public class SimulationData {

	/**
	 * @param args
	 * @throws ModelException 
	 */
	public static void main(String[] args) throws ModelException {
		// Load the SMF 
		Accelerator accelerator = XMLDataManager.acceleratorWithPath("../config/main.xal");
		
		// Define the sequence one is interesting in, in this example MEBT sequence from the SNS accelerator
		String sequenceName = "MEBT";
		// Select only the sequence one is interesting in, from all the components in the accelerator
		AcceleratorSeq sequence = accelerator.getSequence(sequenceName);	
		
		// Instantiation of the scenario object for a given sequence
		Scenario model = Scenario.newScenarioFor(sequence);
		
		// Setting up a valid algorithm for the model and loading a sequence
		EnvelopeTracker tracker = new EnvelopeTracker();
		EnvelopeProbe probe = ProbeFactory.getEnvelopeProbe(sequence, tracker);
		model.setProbe(probe);
		model.setSynchronizationMode(Scenario.SYNC_MODE_DESIGN);
		
		// Run the model
		model.run();
		
		// Retrieve results.
		// Retrieve the trajectory object
		Trajectory trajectory = probe.getTrajectory();
		
		// Retrieve the final state of the trajectory
		ProbeState finalState = trajectory.finalState();
		
		// Print the output of the trajectory
		System.out.println("Trajectory" + trajectory.toString());
		
		// Get an iterator to loop through all the different states
		Iterator<ProbeState> stateItr = trajectory.stateIterator();
		int i = 0;
		while (stateItr.hasNext()){
			i++;
			Object o = stateItr.next();
			System.out.println("State #" + i + ":" + o.toString()); 
		}
		
		// Print just the final state of the envelope at the last position of the simulation
		System.out.println("Final state: " + finalState.toString());
		
		// Print the twiss parameters 
		// This was depricated recently, ToDo find out how to get the twiss parameters
		/*Twiss[] twissArray = probe.getTwiss();
		for (Twiss twiss : twissArray)
			System.out.println("Twiss parameters: " + twiss.toString());*/

		//Get the first-order response matrix accumulated by the Envelope since its initial state.And print the matrix
		System.out.println("Response Matrix:");
		System.out.println(probe.getResponseMatrix());

	}

}
