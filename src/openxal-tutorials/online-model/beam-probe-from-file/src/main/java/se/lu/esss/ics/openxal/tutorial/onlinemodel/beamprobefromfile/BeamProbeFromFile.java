package se.lu.esss.ics.openxal.tutorial.onlinemodel.beamprobefromfile;

import xal.model.probe.Probe;
import xal.model.xml.ParsingException;
import xal.model.xml.ProbeXmlParser;

public class BeamProbeFromFile {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Define where the new probe file is located
		String probeFile = "../config/test.probe";
		Probe probe;
		try {
			probe = ProbeXmlParser.parse(probeFile);
			probe.initialize();
		} catch (ParsingException e) {
			System.out.println("Parsing Exception was thrown\n");
			e.printStackTrace();
			System.exit(1);
		}
		System.out.println("Ends\n");
	}

}
