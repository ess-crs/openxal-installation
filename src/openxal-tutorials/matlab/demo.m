% The openxal.library.jar must be in the MATLAB java classpath.
% please verify this by running the 'javaclasspath' MATLAB command

SEQUENCE = 'mebt'

START_ELEMENT = 'MEBT-PBO_QV-1'
END_ELEMENT   = 'MEBT-PBO_QV-4'

accel = xal.smf.data.XMLDataManager.loadDefaultAccelerator()
seq   = accel.getSequence(SEQUENCE)

start_element = seq.getNodeWithId(START_ELEMENT)
seq.getPosition(start_element)


%---------------------------
% Script Start
%---------------------------
% create and initialize an algorithm
etracker = xal.sim.scenario.AlgorithmFactory.createEnvTrackerAdapt(seq)


% set some custom parameters
etracker.setMaxIterations(10000)
etracker.setAccuracyOrder(1)
etracker.setErrorTolerance(0.001)

% create and initialize a probe
probe = xal.sim.scenario.ProbeFactory.getEnvelopeProbe(seq, etracker)
probe.setBeamCurrent(0.038)
probe.setKineticEnergy(885.e6)
probe.setSpeciesCharge(-1)
probe.setSpeciesRestEnergy(939.29e6)

probe.initialize()

% Create and initialize the model to the target sequence
model = xal.sim.scenario.Scenario.newScenarioFor(seq)



model.setProbe(probe)
model.setSynchronizationMode(xal.sim.scenario.Scenario.SYNC_MODE_DESIGN)
model.resync()
model.setStartNode(START_ELEMENT)

% Run the model
model.run()

% Retrieve the probe from the model and then the trajectory object from the
% probe
probe = model.getProbe()
traj = probe.getTrajectory()

% Retrieve the final state of the simulation
data_final = traj.finalState()

% If the probe is an EnvelopeProbe, we can get the covariance matrix of the
% final state
final_covariance = data_final.getCovarianceMatrix().getArrayCopy()

% put the covariance matrices in a vector
covariance_matrices = zeros(7, 7, traj.numStates)
for i = i:traj.numStates
    probe_state = traj.stateWithIndex(i-1)
    covariance_matrices(:,:,i) = probe_state.getCovarianceMatrix().getArrayCopy()
end

