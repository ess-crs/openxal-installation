package se.lu.esss.ics.openxal.tutorial.xalplot.finalapp;

import java.net.URL;
import java.util.Locale;
import java.util.Scanner;

import xal.extension.application.XalDocument;
import xal.extension.widgets.plot.BasicGraphData;

/**
 * Document class is a holder for document contents.
 * It provides the framework with document specific information 
 * and also provides hooks for document control. 
 * The application framework allows the developer the freedom to encode their document as they wish.
 */
public class Document extends XalDocument {
	protected BasicGraphData data;
	
	/**
	 *  Empty constructor to create empty document
	 */
	public Document() {
		data = new BasicGraphData();
		for (double i = 0.; i < 10.; i += 0.2) {
			data.addPoint(i, Math.sin(i));
		}
	}

	public Document(BasicGraphData data) {
		this.data = data;
	}

	/**
	 * This method is called when the window needs to be shown.
	 * Although dirty, variable _mainWindow has to be set to point to the window representing this document.
	 * @see xal.application.XalAbstractDocument#makeMainWindow()
	 */
	@Override
	protected void makeMainWindow() {	
		mainWindow = new Window(this);
	}

	/**
	 * This method saves the content of this document.
	 * @see xal.application.XalAbstractDocument#saveDocumentAs(java.net.URL)
	 * 
	 * @param url Path to the file to which to save the document.
	 */
	@Override
	public void saveDocumentAs(URL url) {
	}	
	
	/**
	 * This method is provided as a helper in the template to load the content.
	 * 
	 *  @param url path to the file to load
	 * */
	public static Document load(URL url)
	{		
		if (url == null)
			return null;
		try {
			BasicGraphData data = new BasicGraphData();
			Scanner scanner = new Scanner(url.openStream());
			scanner.useLocale(Locale.ROOT);
			while (scanner.hasNextLine()) {
				if (!scanner.hasNext("#")) {
					double x = scanner.nextDouble();
					double y = scanner.nextDouble();
					data.addPoint(x, y);
				}
				if (scanner.hasNextLine())
					scanner.nextLine();
			}
			scanner.close();

			Document doc = new Document(data);
			doc.setHasChanges(false);
			return doc;
		} catch (java.io.IOException exception) {
			throw new RuntimeException(exception.getMessage());
		}		
	}
}
