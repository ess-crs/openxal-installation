package se.lu.esss.ics.openxal.tutorial.acceleratorapp.step3;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.swing.table.AbstractTableModel;

import xal.extension.application.FrameApplication;
import xal.model.ModelException;
import xal.model.alg.EnvelopeTracker;
import xal.model.alg.Tracker;
import xal.model.probe.EnvelopeProbe;
import xal.model.probe.Probe;
import xal.sim.scenario.ModelInput;
import xal.sim.scenario.Scenario;
import xal.smf.AcceleratorNode;
import xal.smf.AcceleratorSeq;
import xal.extension.application.smf.AcceleratorDocument;
import xal.tools.beam.Twiss;

/**
 * Document class is a holder for document contents. It provides the framework
 * with document specific information and also provides hooks for document
 * control. The application framework allows the developer the freedom to encode
 * their document as they wish.
 */
public class Document extends AcceleratorDocument {
	/** Probe used for simulation */
	private Probe probe;
		
	/** Scenario for simulation */
	private Scenario scenario;
	
	/** Modifiable parameters */
	private List<ModelInput> modifiableParameters = new ArrayList<ModelInput>();
		

	/**
	 * Empty constructor to create empty document
	 */
	public Document() {
		setupProbe();
	}

	/**
	 * This method is called when the window needs to be shown. Although dirty,
	 * variable mainWindow has to be set to point to the window representing
	 * this document.
	 * 
	 * @see xal.application.XalAbstractDocument#makeMainWindow()
	 */
	@Override
	protected void makeMainWindow() {
		mainWindow = new Window(this);
	}

	/**
	 * We hardcoded an initial probe that is used. This decision is made for
	 * lazy user so they won't complain when trying out the application.
	 * */
	private void setupProbe() {
		// Envelope probe and tracker
		EnvelopeTracker envelopeTracker = new EnvelopeTracker();
		envelopeTracker.setRfGapPhaseCalculation(false);
		envelopeTracker.setUseSpacecharge(true);
		envelopeTracker.setEmittanceGrowth(false);
		envelopeTracker.setStepSize(0.004);
		envelopeTracker.setProbeUpdatePolicy(Tracker.UPDATE_EXIT);

		EnvelopeProbe envelopeProbe = new EnvelopeProbe();
		envelopeProbe.setAlgorithm(envelopeTracker);
		envelopeProbe.setSpeciesCharge(-1);
		envelopeProbe.setSpeciesRestEnergy(9.39294e8);
		envelopeProbe.setKineticEnergy(2500000);
		envelopeProbe.setPosition(0.0);
		envelopeProbe.setTime(0.0);
		envelopeProbe.initFromTwiss(new Twiss[] {
				new Twiss(-1.62, 0.155, 3.02e-6),
				new Twiss(3.23, 0.381, 3.46e-6),
				new Twiss(0.0196, 0.5844, 3.8638e-6) });
		envelopeProbe.setBeamCurrent(0.02);
		envelopeProbe.setBunchFrequency(4.025e8);

		probe = envelopeProbe;
	}

	/**
	 * This procedure is overridden from AcceleratorApplication and is called
	 * when user selects or changes the sequence. It takes care of regenerating
	 * the scenario and loading modifiableParameters.
	 * */
	@Override
	public void selectedSequenceChanged() {
		AcceleratorSeq seq = getSelectedSequence();

		scenario = null;
		modifiableParameters.clear();

		if (seq != null) {
			try {
				// generates the scenario
				scenario = Scenario.newScenarioFor(seq);
				if (scenario != null) scenario.setProbe(probe);

				// loads modifiable parameters
				for (AcceleratorNode node : seq.getNodes()) {
					try {
						Map<String, Double> props = scenario.propertiesForNode(node);
						if (props != null && !props.isEmpty()) {
							for (Map.Entry<String, Double> mapEntry : props.entrySet()) {
								modifiableParameters.add(new ModelInput(node, mapEntry.getKey(), mapEntry.getValue()));
							}
						}
					} catch (IllegalArgumentException e) {
						// this exception happens on nodes without parameters
						// we catch it, but don't display it
					}
				}
			} catch (ModelException e) {
				FrameApplication.displayError("Generating scenario error",
								"Error while generating scenario & collecting parameters", e);
				scenario = null;
			}
		}

		// updates the gui
		parametersTableModel.fireTableDataChanged();
	}

	
	/********** Table handling code *************/
	
	private ParametersTableModel parametersTableModel = new ParametersTableModel();
	/**
	 * ParametersTableModel class is a bridge to table data.
	 * */
	public class ParametersTableModel extends AbstractTableModel {
		private static final long serialVersionUID = 1L;
		
		private String[] columns = {"Node", "Attribute", "Model value", "New value"}; 
				
		/* Following methods return basic table structure */
		
		@Override
		public int getColumnCount() {
			return columns.length;
		}
		
		@Override
		public int getRowCount() {
			if (modifiableParameters != null)
				return modifiableParameters.size();
			return 0;
		}
		
		@Override
		public String getColumnName(int column) {
			return columns[column];
		}

		/**
		 * Returns table cell value
		 * */
		@Override
		public Object getValueAt(int rowIndex, int columnIndex) {
			if (modifiableParameters != null && scenario != null) {
				ModelInput modelInput = modifiableParameters.get(rowIndex);
				switch (columnIndex) {
				case 0:
					return modelInput.getAcceleratorNode().getId();
				case 1:
					return modelInput.getProperty();
				case 2:
					return modelInput.getDoubleValue();
				case 3:
					ModelInput newModelInput = scenario.getModelInput(modelInput.getAcceleratorNode(),
							modelInput.getProperty());
					if (newModelInput != null)
						return newModelInput.getDoubleValue();
					else
						return null;
				}
			}
			return "";
		}
	}
	
	public ParametersTableModel getParametersTableModel() {
		return parametersTableModel;
	}

	/**
	 * This method saves the content of this document.
	 * 
	 * @see xal.application.XalAbstractDocument#saveDocumentAs(java.net.URL)
	 * 
	 * @param url Path to the file to which to save the document.
	 */
	@Override
	public void saveDocumentAs(URL url) {
		// TODO Implement code to save the document
	}	

	/**
	 * This method is provided as a helper in the template to load the content.
	 * 
	 * @param url path to the file to load
	 * */
	public static Document load(URL url) {
		Document document = new Document();
		// TODO implement code to load the document
		return document;
	}	
}
