package se.lu.esss.ics.py4jserver;

import java.util.ArrayList;
import java.util.List;

import javax.sql.CommonDataSource;

import org.apache.commons.collections.CollectionUtils;

import py4j.GatewayServer;
import xal.model.alg.EnvTrackerAdapt;
import xal.model.probe.EnvelopeProbe;
import xal.model.probe.traj.EnvelopeTrajectory;
import xal.model.probe.traj.ProbeState;
import xal.sim.scenario.ProbeFactory;
import xal.smf.AcceleratorSeq;

public class Py4JServer {

    public static void main(String[] args) {
        GatewayServer gatewayServer = new GatewayServer(null);
        gatewayServer.start();
        System.out.println("Gateway Server Started");
    }
    
    /* Helper functions */

    /*
     * This is a wrapper around ProbeFactory using a concrete type
     */
    public static EnvelopeProbe getEnvelopeProbe(final AcceleratorSeq sequence, final EnvTrackerAdapt envTracker) {
        return ProbeFactory.getEnvelopeProbe(sequence, envTracker);
    }
    
    /*
     * Helper function that will create a list of ProbeStates for the trajectory
     * 
     * The list is natively iterable in Python.
     */
    @SuppressWarnings("rawtypes")
    public static List<ProbeState> listFromEnvelopeTrajectory(EnvelopeTrajectory trajectory) {
        ArrayList collection = new ArrayList();
        CollectionUtils.addAll(collection, trajectory.iterator());
        return collection;
    }
}
