#!/bin/bash
#
# Author: ivo.list@cosylab.com
#

# Execute everything in the base dir of the script

cd "`dirname "${BASH_SOURCE[0]}"`" > /dev/null

# Environment..
. openxal-environment.sh

# Install OpenXAL:
./install-update.sh

# 1. Configure OpenXAL Python bridge
cd jpype_config
# We assume that if user is root, then we want to install system wide
# otherwise we install for only the user executing..
if [[ $UID -eq 0 ]]
then
    python setup.py install
else
    python setup.py install --user --prefix=
fi

