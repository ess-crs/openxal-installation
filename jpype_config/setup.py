#!/usr/bin/env python
# -*- coding: utf-8 -*-

# -- First we have to configure the __init__.py from the macro,
#  based on required environment variables

from setuptools import setup,find_packages

import os
import sys

# Path to installation:
OXAL_HOME=os.environ['OPENXAL_HOME']
JAVA_HOME=os.environ['JAVA_HOME']

# Path to the Java used to compile OpenXAL:
for path in ['jre/lib/server/libjvm.dylib',
             'jre/lib/amd64/server/libjvm.so']:
    JAVA_JVM=os.path.join(JAVA_HOME,path)
    if os.path.exists(JAVA_JVM):
        break

# The version of OpenXAL
OXAL_VERSION=os.environ['OPENXAL_VERSION']

# The OpenXAL jar file:
OXAL_LIBRARY=os.environ['OPENXAL_LIBRARY']

# Check that variables make sense
if not os.path.exists(JAVA_JVM):
    raise ValueError("We were not able to find a Java installation, please set JAVA_HOME")
if not os.path.exists(OXAL_HOME):
    raise ValueError("We could not find an OpenXAL installation at OPENXAL_HOME = {}".format(OPENXAL_HOME))
if not os.path.exists(OXAL_LIBRARY):
    raise ValueError("We could not find the OpenXAL library at OPENXAL_LIBRARY = {}".format(OPENXAL_LIBRARY))

text=file('oxal/__init__.py.macro','r').read()
formatted=text.format(**locals())
file('oxal/__init__.py','w').write(formatted)


# -- The rest is a simple setup

setup(
        name='OpenXAL',
        version=OXAL_VERSION,
        description='Python config for OpenXAL',
        url='http://xaldev.sourceforge.net/',
        install_requires=['JPype1==0.6.1'],
        packages=find_packages(),
)
