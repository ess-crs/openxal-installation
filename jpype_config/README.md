Need the following environment variables, either set manually or using openxal-environment.sh:

```
export OPENXAL_HOME=$HOME/Programming/openxal/
export OPENXAL_VERSION=1.0.5-SNAPSHOT
export OPENXAL_LIBRARY=$OPENXAL_HOME/library/target/openxal.library-${OPENXAL_VERSION}.jar
```

Then run the commands:

```
python create.py
python setup.py install --user --prefix=
```
