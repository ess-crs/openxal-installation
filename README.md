
PREREQUISITES

Java version >= 1.8

INSTALLATION

There is a dual purpose script called "install-update.sh". On the first run
it will install everything necessary to run openxal.

On consecutive runs it will update ESS's distribution of OpenXAL to the latest 
version.

RUNNING

Try "bin/launcher.sh" or any other of the provided scripts.

SCRIPTING via JYTHON

Before running scripts, environment should be set up correctly.
To do this run: "source openxal-environment.sh".

After that you can start for example:
jython src/openxal-tutorials/jython/demo.py

SCRIPTING via JPYPE

JPYPE is a python module. You can install by calling "install-update-jpype.sh".
Required is Python version >= 2.6.

Test script may be run using:
python src/openxal-tutorials/jpype/test.py

If installation of Java SE 6 is required please check: http://goo.gl/0mWMBl

SOURCES

In current distribution only tutorial projects are provided. To inspect
OpenXAL's source code run script "fetch-sources.sh" which will download
latest source into src directory. For a more serious development "git" 
should be installed and configured.

