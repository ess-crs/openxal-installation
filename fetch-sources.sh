#!/bin/bash
#
# Script clones most important git reposities of OpenXal
#

cd $(dirname "${BASH_SOURCE[0]}")/src

../git/git clone https://bitbucket.org/europeanspallationsource/openxal.git

../git/git clone https://bitbucket.org/europeanspallationsource/openxal-tutorials.git

../git/git clone https://bitbucket.org/europeanspallationsource/openxal-deps.git

