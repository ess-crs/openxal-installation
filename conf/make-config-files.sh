#!/bin/bash
#
# Makes configuration files using absolute paths (because relative paths don't behave well).
#
# Author: ivo.list@cosylab.com
#

. ./openxal-environment.sh

ETC=$OPENXAL_CONFIG_DIR

echo "Generating configuration files with absolute paths."

echo 'DEFAULT_DOCUMENT = file:'$ETC'/main.launch' > $ETC/xal.app.launcher
echo 'DEFAULT_FOLDER = "file:'$ETC'"' > $ETC/xal.extension.smf.application
echo 'DEFAULT_FOLDER = "file:'$ETC'"' > $ETC/xal.extension.application
echo 'mainPath = '$OPENXAL_HOME'/optics/design/main.xal' > $ETC/xal.smf.data

